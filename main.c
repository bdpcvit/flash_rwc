#include "io430.h"
#include <stdint.h>
#include <intrinsics.h>

#define RED_ON  P1OUT |= BIT0
#define RED_OFF  P1OUT &= ~BIT0
#define GREEN_ON  P1OUT |= BIT6
#define GREEN_OFF  P1OUT &= ~BIT6

// User types defenitions
typedef enum
{
  F_OK = 1,
  F_ERROR
}error_t;


// Function declaration
error_t Clear_Flash_Block(uint8_t *var);
error_t Write_Flash_Byte(uint8_t *point, uint8_t var);
error_t Write_Flash_Word(uint16_t *point, uint16_t var);

//Global variabless

uint8_t * const test1 = (uint8_t *)(0xc000 + (4 * 512));
uint16_t * const test2 = (uint16_t *)(0xc000 + (4 * 512) + sizeof(*test1) + sizeof(uint16_t));





void main(void)
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;
  
  P1DIR |= BIT0 | BIT6;
  GREEN_OFF;
  RED_OFF;
  
  BCSCTL3 = LFXT1S1;
  DCOCTL = 0x00;			//Clear DCOCTL register
  DCOCTL = CALDCO_1MHZ;
  BCSCTL1 = CALBC1_1MHZ;
  
  
  
  if(Clear_Flash_Block(test1) == F_ERROR) RED_ON;
  if(Write_Flash_Byte(test1, 0xBB) == F_ERROR) GREEN_ON;
  if(Write_Flash_Word(test2, 0xAAAA) == F_ERROR) GREEN_ON;
  
  while (1);
}



error_t Clear_Flash_Block(uint8_t *var)
{
  FCTL2 = FWKEY | FSSEL_1 | FN0 | FN1;	//Flash clock select MCLK
  //FSSEL_0 (0x0000u)  /* Flash clock select: 0 - ACLK */
  //FSSEL_1 (0x0040u)  /* Flash clock select: 1 - MCLK */
  //FSSEL_2 (0x0080u)  /* Flash clock select: 2 - SMCLK */
  //FSSEL_3 (0x00C0u)  /* Flash clock select: 3 - SMCLK */
  /* FN0-FN5  Divide Flash clock by 1 to 64 using FN0 to FN5 according to: */
  /* 32*FN5 + 16*FN4 + 8*FN3 + 4*FN2 + 2*FN1 + FN0 + 1 */
  FCTL3 = FWKEY;
  FCTL1 = FWKEY | ERASE;
  *var = 0;
  
  
    if (FCTL3 & FAIL)
    {
      return F_ERROR;
    }else
    {
      FCTL3 = FWKEY + LOCK;
      return F_OK;
    }
}


error_t Write_Flash_Byte(uint8_t *point, uint8_t var)
{
  FCTL2 = FWKEY | FSSEL_1 | FN0 | FN1;	//Flash clock select MCLK
  //FSSEL_0 (0x0000u)  /* Flash clock select: 0 - ACLK */
  //FSSEL_1 (0x0040u)  /* Flash clock select: 1 - MCLK */
  //FSSEL_2 (0x0080u)  /* Flash clock select: 2 - SMCLK */
  //FSSEL_3 (0x00C0u)  /* Flash clock select: 3 - SMCLK */
  /* FN0-FN5  Divide Flash clock by 1 to 64 using FN0 to FN5 according to: */
  /* 32*FN5 + 16*FN4 + 8*FN3 + 4*FN2 + 2*FN1 + FN0 + 1 */
  FCTL3 = FWKEY;
  FCTL1 = FWKEY | WRT;
  *point = var;
      
    if (FCTL3 & FAIL)
    {
      return F_ERROR;
    }else
    {
      FCTL3 = FWKEY + LOCK;
      FCTL1 = FWKEY;
      return F_OK;
    }
}


error_t Write_Flash_Word(uint16_t *point, uint16_t var)
{
  FCTL2 = FWKEY | FSSEL_1 | FN0 | FN1;	//Flash clock select MCLK
  //FSSEL_0 (0x0000u)  /* Flash clock select: 0 - ACLK */
  //FSSEL_1 (0x0040u)  /* Flash clock select: 1 - MCLK */
  //FSSEL_2 (0x0080u)  /* Flash clock select: 2 - SMCLK */
  //FSSEL_3 (0x00C0u)  /* Flash clock select: 3 - SMCLK */
  /* FN0-FN5  Divide Flash clock by 1 to 64 using FN0 to FN5 according to: */
  /* 32*FN5 + 16*FN4 + 8*FN3 + 4*FN2 + 2*FN1 + FN0 + 1 */
  FCTL3 = FWKEY;
  FCTL1 = FWKEY | WRT;
  *point = var;
      
    if (FCTL3 & FAIL)
    {
      return F_ERROR;
    }else
    {
      FCTL3 = FWKEY + LOCK;
      FCTL1 = FWKEY;
      return F_OK;
    }
}
